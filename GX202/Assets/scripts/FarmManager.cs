using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FarmManager : MonoBehaviour
{
    public Item selectPlant;
    public bool isPlanting = false;

    public int money = 100;
    public Text moneyTxt;

    public Color buyColour = Color.green;
    public Color cancelColour = Color.red;

    void Start()
    {
        moneyTxt.text = "$" + money;
    }





    public void SelectPlant(Item newPlant)//what is selected
    {
        //deselect
        if (selectPlant == newPlant)
        {
            selectPlant.btnImage.color = buyColour;
            selectPlant.btnTxt.text = "Buy";
            selectPlant = null;
            isPlanting = false;

        }
        //select seed
        else
        {
            if (selectPlant != null)
            {
                selectPlant.btnImage.color = buyColour;
                selectPlant.btnTxt.text = "Buy";
            }

            selectPlant = newPlant;
            selectPlant.btnImage.color = cancelColour;
            selectPlant.btnTxt.text = "Cancel";
            isPlanting = true;
        }
    }

    public void Transaction(int value)
    {
        money += value;
        moneyTxt.text = "$" + money;
    }
}
