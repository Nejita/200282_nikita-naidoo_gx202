using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolSelectScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnMouseDown()
    {
        //scythe is selected
        if (gameObject.name == "scythe")
        {
            GMScript.currentTool = "scythe";
            Debug.Log(" GMScript.currentTool = 'scythe'");
        }

        //blueberry seed is selected
        if (gameObject.name == "blueberrySeedBag")
        {
            GMScript.currentTool = "blueberrySeedBag";
            Debug.Log(" GMScript.currentTool = 'blueberrySeedBag'");
        }
    }
}
