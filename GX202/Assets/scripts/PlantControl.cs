using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantControl : MonoBehaviour
{

    [SerializeField] bool noPlant = false;
    [SerializeField] bool canHarvest = true;
    public Sprite noPlantObj;
    public Sprite blueberryStage1;
    public Sprite blueberryStage2;
    public Sprite blueberryStage3;

    public float growTime = 0;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        BlueBerryGrowth();
    }

    private void OnMouseDown()
    {
//        Debug.Log("Clicked on soil plot");

        //use scythe
        if (GMScript.currentTool == "scythe" && canHarvest)
        {
            //swap sprite to show empty plot and set no plant bool check to no plant = true
            GetComponent<SpriteRenderer>().sprite = noPlantObj;
            Debug.Log("harvest successful");
            noPlant = true;
            canHarvest = false;
        }

        //plant blueberry
        if (GMScript.currentTool == "blueberrySeedBag" && noPlant)
        {
            //swap sprite to blueberry sprout and set no plant bool check to false
            GetComponent<SpriteRenderer>().sprite = blueberryStage1;
            Debug.Log("blueberry successfully planted");
            noPlant = false;
            canHarvest = false;
        }

    }
    private void BlueBerryGrowth()
    {
        if (GetComponent<SpriteRenderer>().sprite == blueberryStage1)
        {
            growTime += Time.deltaTime;
        }

        if (growTime > 3)
        {
            GetComponent<SpriteRenderer>().sprite = blueberryStage2;
            growTime += Time.deltaTime;
        }
        if (growTime > 7)
        {
            GetComponent<SpriteRenderer>().sprite = blueberryStage3;
            canHarvest = true;
        }
    }
}
