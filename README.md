# 200282_Nikita Naidoo_GX202

GX202 -  a farming and selling based system 

The aim of this system was to create a plant system that selects a seed type and where to plant it. The seed should grow depending on the plant and only harvested when fully grown. Press cancel to deselect seed and harvest. You should earn money when harvesting. Amount earned is dependent on the plant value. To plant, seeds must be bought using the cash owned. 
